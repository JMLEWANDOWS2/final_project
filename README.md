# Final_project
This is my final project for AOS 573

# Files
environment.yml - Python environment for this project
IBTrACS.NA.v04r00.nc - Dataset for this project, (https://www.ncei.noaa.gov/products/international-best-track-archive)
Final_Project.ipynb - Jupyter notebook with the final project
README.md - This file